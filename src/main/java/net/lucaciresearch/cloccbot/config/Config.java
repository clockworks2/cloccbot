/*
 * Cloccbot - Clockworks Minute Bot
 * Copyright (C) 2023-present Lucaci Research
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *  ||===========================================================================||
 *  ||  _                          _  ______                              _      ||
 *  || | |                        (_) | ___ \                            | |     ||
 *  || | |    _   _  ___ __ _  ___ _  | |_/ /___  ___  ___  __ _ _ __ ___| |__   ||
 *  || | |   | | | |/ __/ _` |/ __| | |    // _ \/ __|/ _ \/ _` | '__/ __| '_ \  ||
 *  || | |___| |_| | (_| (_| | (__| | | |\ \  __/\__ \  __/ (_| | | | (__| | | | ||
 *  || \_____/\__,_|\___\__,_|\___|_| \_| \_\___||___/\___|\__,_|_|  \___|_| |_| ||
 *  ||                                                                           ||
 *  ||===========================================================================||
 *
 *
 *                                 //===========\\
 *                                //      _ ____ \\
 *                               //      | |  _ \ \\
 *                              //       | | |_) | \\
 *                              \\   __  | |  _ <  //
 *                               \\ |__| |_|_| \_\//
 *                                \\             //
 *                                 \\===========//
 *
 */

package net.lucaciresearch.cloccbot.config;

import com.google.common.io.Resources;
import lombok.Getter;
import org.apache.commons.lang3.SystemUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class Config {

    private static final Logger logger = LoggerFactory.getLogger(Config.class);

    private static final Path windowsConfigFolder = Path.of("Cloccbot");
    private static final Path linuxConfigFolder = Path.of( "Cloccbot");
    private static final Path jsonConfigFile = Path.of("config.json");
    private static final Path hiberanteDBFile = Path.of("entities");
    private static final Path quartzDBFile = Path.of("timers");

    private static final Config ourInstance = new Config();
    public static Config getInstance() { return ourInstance; }
    private Config() {}

    @Getter
    private String discordToken;

    @Getter
    private String guildId;

    @Getter
    private String jdbcQuartzDBString;

    @Getter
    private String jdbcHibernateDBString;

    @Getter
    private String wikiUploadToken;

    @Getter
    private String wikiUploadUrl;

    @Getter
    private String minuteUrlPrefix;

    @Getter
    private String shortenerUrl;

    @Getter
    private TextChannelMapping discordChannelMapping;

    @Getter
    private Emotes emotes;

    public boolean initialize() {
        Path configFolder = null;
        if (SystemUtils.IS_OS_WINDOWS_7 || SystemUtils.IS_OS_WINDOWS_8 || SystemUtils.IS_OS_WINDOWS_10) {
            configFolder = Path.of(System.getProperty("user.home")).resolve(windowsConfigFolder);
        }
        if (SystemUtils.IS_OS_LINUX) {
            configFolder = Path.of(System.getProperty("user.home")).resolve(linuxConfigFolder);
        }
        if (configFolder == null) {
            logger.error("Your OS ({}) is not supported", System.getProperty("os.name"));
            return false;
        }

        if (!Files.exists(configFolder) || !Files.isDirectory(configFolder)) {
            try {
                Files.deleteIfExists(configFolder);
                Files.createDirectories(configFolder);
            } catch (IOException e) {
                logger.error("Could not create config folder at " + configFolder.toString(), e);
                return false;
            }
        }

        Path configFile = configFolder.resolve(jsonConfigFile);
        if (!Files.exists(configFile) || !Files.isRegularFile(configFile)) {
            try {
                Files.deleteIfExists(configFile);
                Files.writeString(configFile, Resources.toString(Resources.getResource(jsonConfigFile.toString()), StandardCharsets.UTF_8));
            } catch (IOException e) {
                logger.error("Could not create config file at " + configFile.toString(), e);
                return false;
            }
        }

        Path quartzDB = configFolder.resolve(quartzDBFile);
        Path quartzDBPath = configFolder.resolve(quartzDBFile.toString() + ".mv.db");
        if (!Files.exists(quartzDBPath) || !Files.isRegularFile(quartzDBPath)) {
            try {
                Files.deleteIfExists(quartzDBPath);
                Resources.asByteSource(Resources.getResource(quartzDBFile.toString() + ".mv.db")).copyTo(com.google.common.io.Files.asByteSink(new File(quartzDBPath.toString())));
            } catch (IOException e) {
                logger.error("Could not create quartz database at " + quartzDB.toString(), e);
                return false;
            }
        }

        try {
            String configString = Files.readString(configFile);
            JSONObject conf = new JSONObject(configString);

            discordToken = conf.getString("discordToken");
            guildId = conf.getString("guildId");
            wikiUploadToken = conf.getString("wikiUploadToken");
            minuteUrlPrefix = conf.getString("minuteUrlPrefix");
            wikiUploadUrl = conf.getString("wikiUploadUrl");
            shortenerUrl = conf.getString("shortenerUrl");

            if (!minuteUrlPrefix.endsWith("/")) minuteUrlPrefix = minuteUrlPrefix + "/";

            discordChannelMapping = new TextChannelMapping(conf.getJSONObject("textChannels"));
            emotes = new Emotes(conf.getJSONObject("emotes"));

        } catch (JSONException e) {
            logger.error("Failed to parse json config file. Please check the syntax", e);
            return false;
        } catch (IOException e) {
            logger.error("Cannot read config file at " + configFile.toString(), e);
            return false;
        }

        jdbcHibernateDBString = "jdbc:h2:" + configFolder.resolve(hiberanteDBFile).toString();
        jdbcQuartzDBString = "jdbc:h2:" + quartzDB.toString();

        return true;
    }

}
