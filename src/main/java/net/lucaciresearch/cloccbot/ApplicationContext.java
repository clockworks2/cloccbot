/*
 * Cloccbot - Clockworks Minute Bot
 * Copyright (C) 2023-present Lucaci Research
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *  ||===========================================================================||
 *  ||  _                          _  ______                              _      ||
 *  || | |                        (_) | ___ \                            | |     ||
 *  || | |    _   _  ___ __ _  ___ _  | |_/ /___  ___  ___  __ _ _ __ ___| |__   ||
 *  || | |   | | | |/ __/ _` |/ __| | |    // _ \/ __|/ _ \/ _` | '__/ __| '_ \  ||
 *  || | |___| |_| | (_| (_| | (__| | | |\ \  __/\__ \  __/ (_| | | | (__| | | | ||
 *  || \_____/\__,_|\___\__,_|\___|_| \_| \_\___||___/\___|\__,_|_|  \___|_| |_| ||
 *  ||                                                                           ||
 *  ||===========================================================================||
 *
 *
 *                                 //===========\\
 *                                //      _ ____ \\
 *                               //      | |  _ \ \\
 *                              //       | | |_) | \\
 *                              \\   __  | |  _ <  //
 *                               \\ |__| |_|_| \_\//
 *                                \\             //
 *                                 \\===========//
 *
 */

package net.lucaciresearch.cloccbot;

import lombok.Getter;
import net.lucaciresearch.cloccbot.data.DataRepositoryFactory;
import net.lucaciresearch.cloccbot.discord.Discord;
import net.lucaciresearch.cloccbot.minute.MinuteActions;
import net.lucaciresearch.cloccbot.minute.MinuteBuilderFactory;
import net.lucaciresearch.cloccbot.scheduler.QuartzScheduler;
import net.lucaciresearch.cloccbot.urls.UrlShortener;
import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.login.LoginException;

public class ApplicationContext {
    private static final Logger logger = LoggerFactory.getLogger(ApplicationContext.class);

    @Getter
    private final OkHttpClient httpClient;

    @Getter
    private final MinuteBuilderFactory minuteBuilderFactory;

    @Getter
    private final QuartzScheduler scheduler;

    @Getter
    private final DataRepositoryFactory dataFactory;

    @Getter
    private final Discord discord;

    @Getter
    private final MinuteActions minuteActions;

    @Getter
    private final UrlShortener urlShortener;


    public ApplicationContext() throws Exception {
        httpClient = new OkHttpClient();
        minuteBuilderFactory = new MinuteBuilderFactory(this);
        scheduler = new QuartzScheduler(this);
        dataFactory = new DataRepositoryFactory(this);
        minuteActions = new MinuteActions(this);
        urlShortener = new UrlShortener(this);

        try {
            discord = new Discord(this);
        } catch (LoginException loginException) {
            logger.error("Failed to initialize discord", loginException);
            throw new Exception(loginException);
        }

    }
}
