/*
 * Cloccbot - Clockworks Minute Bot
 * Copyright (C) 2023-present Lucaci Research
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *  ||===========================================================================||
 *  ||  _                          _  ______                              _      ||
 *  || | |                        (_) | ___ \                            | |     ||
 *  || | |    _   _  ___ __ _  ___ _  | |_/ /___  ___  ___  __ _ _ __ ___| |__   ||
 *  || | |   | | | |/ __/ _` |/ __| | |    // _ \/ __|/ _ \/ _` | '__/ __| '_ \  ||
 *  || | |___| |_| | (_| (_| | (__| | | |\ \  __/\__ \  __/ (_| | | | (__| | | | ||
 *  || \_____/\__,_|\___\__,_|\___|_| \_| \_\___||___/\___|\__,_|_|  \___|_| |_| ||
 *  ||                                                                           ||
 *  ||===========================================================================||
 *
 *
 *                                 //===========\\
 *                                //      _ ____ \\
 *                               //      | |  _ \ \\
 *                              //       | | |_) | \\
 *                              \\   __  | |  _ <  //
 *                               \\ |__| |_|_| \_\//
 *                                \\             //
 *                                 \\===========//
 *
 */

package net.lucaciresearch.cloccbot.data.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.experimental.Accessors;
import net.lucaciresearch.cloccbot.ApplicationContext;

import java.time.LocalDate;
import java.util.Set;

@SuppressWarnings("JpaObjectClassSignatureInspection")
@Entity
@Accessors(chain = true)
public class CheckinMessage {

    @Transient
    private final ApplicationContext appcon;

    @Id
    private String id;

    @Getter
    @Lob @Convert(converter = LongListConverter.class)
    private Set<Long> participants;


    public CheckinMessage(String id, ApplicationContext appcon) {
        this.appcon = appcon;
        this.id = id;
    }

    public CheckinMessage(ApplicationContext appcon, String id, String meetName, String meedId, String meetLocation) {
        this.appcon = appcon;
        this.id = id;
        this.meetName = meetName;
        this.meedId = meedId;
        this.meetLocation = meetLocation;
    }

    @Getter
    private String meetName;

    @Getter
    private String meedId;

    @Getter
    private String meetLocation;

    @Getter
    private LocalDate localDate;

    @Getter
    private Long noticeMessageId;

    @Getter
    private boolean noticePosted = false;

    @Getter
    private boolean meetEnded = false;

    private CheckinMessage postNotice(long id) {
        noticePosted = true;
        noticeMessageId = id;
        return this;
    }

    private CheckinMessage meetEnd() {
        meetEnded = true;
        return this;
    }

}
