/*
 * Cloccbot - Clockworks Minute Bot
 * Copyright (C) 2023-present Lucaci Research
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *  ||===========================================================================||
 *  ||  _                          _  ______                              _      ||
 *  || | |                        (_) | ___ \                            | |     ||
 *  || | |    _   _  ___ __ _  ___ _  | |_/ /___  ___  ___  __ _ _ __ ___| |__   ||
 *  || | |   | | | |/ __/ _` |/ __| | |    // _ \/ __|/ _ \/ _` | '__/ __| '_ \  ||
 *  || | |___| |_| | (_| (_| | (__| | | |\ \  __/\__ \  __/ (_| | | | (__| | | | ||
 *  || \_____/\__,_|\___\__,_|\___|_| \_| \_\___||___/\___|\__,_|_|  \___|_| |_| ||
 *  ||                                                                           ||
 *  ||===========================================================================||
 *
 *
 *                                 //===========\\
 *                                //      _ ____ \\
 *                               //      | |  _ \ \\
 *                              //       | | |_) | \\
 *                              \\   __  | |  _ <  //
 *                               \\ |__| |_|_| \_\//
 *                                \\             //
 *                                 \\===========//
 *
 */

package net.lucaciresearch.cloccbot.data.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import net.dv8tion.jda.api.utils.TimeUtil;
import net.lucaciresearch.cloccbot.ApplicationContext;
import net.lucaciresearch.cloccbot.data.IdListConverter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

@SuppressWarnings("JpaObjectClassSignatureInspection")
@Entity(name = "minute_table")
@Accessors(chain = true)
public class Minute {
    private static final DateTimeFormatter titleFormatter = DateTimeFormatter.ofPattern("dd MMMM yyyy");
    private static final DateTimeFormatter filenameFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy_HH-mm-ss");

    @Transient
    private final ApplicationContext applicationContext;


    public Minute(String id, ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        this.id = id;
    }

    public Minute(long initiatorId, ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
        this.initiatorId = initiatorId;
        this.id = UUID.randomUUID().toString();
        var time = LocalDateTime.ofInstant(TimeUtil.getTimeCreated(initiatorId).toInstant(), ZoneId.of("GMT+2"));
        navigationOrderIndex = Integer.MAX_VALUE - (int)(time.atZone(ZoneId.systemDefault()).toEpochSecond());
        this.preTitle = titleFormatter.format(time);
        this.filename = filenameFormatter.format(time);
    }

    @Getter
    long initiatorId;

    long initialBotMessageId = -1;

    @Getter @Id
    String id;

    /*
     * This is a list of messages that *could* be part of the minute
     * This list is meant to be updated at every message sent.
     */
    @Lob @Convert(converter = IdListConverter.class)
    List<Long> eligibleMessageIds = new ArrayList<>();

    @Lob @Convert(converter = IdListConverter.class)
    List<Long> auxiliarryMessagesIds = new ArrayList<>();

    @Getter
    boolean active = true;

    @Getter
    boolean successful = false;

    @Setter
    String title = "";

    @Getter
    String preTitle;

    @Getter
    String filename;

    @Getter
    long navigationOrderIndex;

    public String getTitle() {
        return preTitle + (title != null && !title.isEmpty() && !title.isBlank() ? " - " + title : "");
    }

    public void addEligibleMessage(long msgId) {
        if (!eligibleMessageIds.contains(msgId)) {
            eligibleMessageIds.add(msgId);
        }
    }

    public void addAuxiliraryMessage (long msgId) {
        if (!auxiliarryMessagesIds.contains(msgId)) {
            auxiliarryMessagesIds.add(msgId);
        }
    }

    public void setInitialBotMessageId(long id) {
        initialBotMessageId = id;
        addAuxiliraryMessage(id);
    }

    public Optional<Long> getInitialBotMessageId() {
        return initialBotMessageId > 0 ? Optional.of(initialBotMessageId) : Optional.empty();
    }


    public Stream<Long> streamEligibleMessages() {
        return eligibleMessageIds.stream();
    }

    public Stream<Long> streamAuxiliaryMessages() {
        return auxiliarryMessagesIds.stream();
    }

    public void cancel() {
        active = false;
        successful = false;
    }

    public void succeed() {
        active = false;
        successful = true;
    }


}
