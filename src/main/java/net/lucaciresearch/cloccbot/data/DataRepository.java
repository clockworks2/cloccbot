/*
 * Cloccbot - Clockworks Minute Bot
 * Copyright (C) 2023-present Lucaci Research
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *  ||===========================================================================||
 *  ||  _                          _  ______                              _      ||
 *  || | |                        (_) | ___ \                            | |     ||
 *  || | |    _   _  ___ __ _  ___ _  | |_/ /___  ___  ___  __ _ _ __ ___| |__   ||
 *  || | |   | | | |/ __/ _` |/ __| | |    // _ \/ __|/ _ \/ _` | '__/ __| '_ \  ||
 *  || | |___| |_| | (_| (_| | (__| | | |\ \  __/\__ \  __/ (_| | | | (__| | | | ||
 *  || \_____/\__,_|\___\__,_|\___|_| \_| \_\___||___/\___|\__,_|_|  \___|_| |_| ||
 *  ||                                                                           ||
 *  ||===========================================================================||
 *
 *
 *                                 //===========\\
 *                                //      _ ____ \\
 *                               //      | |  _ \ \\
 *                              //       | | |_) | \\
 *                              \\   __  | |  _ <  //
 *                               \\ |__| |_|_| \_\//
 *                                \\             //
 *                                 \\===========//
 *
 */

package net.lucaciresearch.cloccbot.data;

import jakarta.persistence.NoResultException;
import jakarta.persistence.PersistenceException;
import net.lucaciresearch.cloccbot.ApplicationContext;
import net.lucaciresearch.cloccbot.data.entities.Minute;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;
import java.util.function.Supplier;

public class DataRepository implements AutoCloseable {
    public static final Logger logger = LoggerFactory.getLogger(DataRepository.class);

    private final ApplicationContext appcon;
    private final Session session;

    private Transaction tx = null;

    public DataRepository(Session session, ApplicationContext appcon) {
        this.session = session;
        this.appcon = appcon;
    }

    public void beginTransaction() {
        if (tx != null) {
            return;
        }
        tx = session.beginTransaction();
    }

    public void commitTransaction() {
        if (tx == null) {
            return;
        }
        tx.commit();
        tx = null;
    }

    public void rollbackTransaction() {
        if (tx == null) {
            return;
        }
        tx.rollback();
        tx = null;
    }


    public Optional<Minute> getLatestActiveMinute() {
        Query<Minute> tq = session.createQuery("SELECT m FROM Minute m where m.active = true order by m.initiatorId", Minute.class);
        tq.setMaxResults(1);
        try {
            return Optional.ofNullable(tq.getSingleResult());
        } catch (NoResultException noex) {
            return Optional.empty();
        } catch (IllegalStateException | PersistenceException ex) {
            logger.error("Could not fetch latest active minute", ex);
            return Optional.empty();
        }
    }

    public Optional<Minute> getMinuteById(String id) {
        return Optional.ofNullable(session.get(Minute.class, id));
    }

    public Minute createMinute(Supplier<Minute> supplier) {
        boolean trx = tx == null;
        if (trx) beginTransaction();
        Minute m = supplier.get();
        session.persist(m);
        if (trx) commitTransaction();
        return m;
    }

    @Override
    public void close() {
        session.close();
    }

}
