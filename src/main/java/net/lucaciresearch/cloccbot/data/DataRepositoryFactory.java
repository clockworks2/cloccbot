/*
 * Cloccbot - Clockworks Minute Bot
 * Copyright (C) 2023-present Lucaci Research
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *  ||===========================================================================||
 *  ||  _                          _  ______                              _      ||
 *  || | |                        (_) | ___ \                            | |     ||
 *  || | |    _   _  ___ __ _  ___ _  | |_/ /___  ___  ___  __ _ _ __ ___| |__   ||
 *  || | |   | | | |/ __/ _` |/ __| | |    // _ \/ __|/ _ \/ _` | '__/ __| '_ \  ||
 *  || | |___| |_| | (_| (_| | (__| | | |\ \  __/\__ \  __/ (_| | | | (__| | | | ||
 *  || \_____/\__,_|\___\__,_|\___|_| \_| \_\___||___/\___|\__,_|_|  \___|_| |_| ||
 *  ||                                                                           ||
 *  ||===========================================================================||
 *
 *
 *                                 //===========\\
 *                                //      _ ____ \\
 *                               //      | |  _ \ \\
 *                              //       | | |_) | \\
 *                              \\   __  | |  _ <  //
 *                               \\ |__| |_|_| \_\//
 *                                \\             //
 *                                 \\===========//
 *
 */

package net.lucaciresearch.cloccbot.data;

import net.lucaciresearch.cloccbot.ApplicationContext;
import net.lucaciresearch.cloccbot.config.Config;
import net.lucaciresearch.cloccbot.data.entities.Minute;
import net.lucaciresearch.cloccbot.data.entities.SerializationInterceptor;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

public class DataRepositoryFactory {
    private final ApplicationContext appcon;
    private final SessionFactory sessionFactory;

    public DataRepositoryFactory(ApplicationContext appcon) {
        this.appcon = appcon;
        Configuration cfg = new Configuration()
                //.addAnnotatedClass(Thread.class)
                .addAnnotatedClass(Minute.class)
                .setInterceptor(new SerializationInterceptor(appcon));
        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(HibernateH2Parameters.get(Config.getInstance().getJdbcHibernateDBString()))
                .build();
        sessionFactory = cfg.buildSessionFactory(serviceRegistry);
    }

    public DataRepository create() {
        return new DataRepository(sessionFactory.openSession(), appcon);
    }

}

interface HibernateH2Parameters {
    static Properties get(String h2url) {
        Properties hProps = new Properties();
        hProps.setProperty("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        hProps.setProperty("hibernate.connection.driver_class", "org.h2.Driver");
        hProps.setProperty("hibernate.connection.url", h2url);
        hProps.setProperty("hibernate.connection.username", "sa");
        hProps.setProperty("hibernate.connection.password", "");
        hProps.setProperty("hibernate.hbm2ddl.auto", "update");
        hProps.setProperty("hibernate.show_sql", "false");
        return hProps;
    }
}