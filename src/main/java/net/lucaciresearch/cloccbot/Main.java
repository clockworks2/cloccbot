/*
 * Cloccbot - Clockworks Minute Bot
 * Copyright (C) 2023-present Lucaci Research
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *  ||===========================================================================||
 *  ||  _                          _  ______                              _      ||
 *  || | |                        (_) | ___ \                            | |     ||
 *  || | |    _   _  ___ __ _  ___ _  | |_/ /___  ___  ___  __ _ _ __ ___| |__   ||
 *  || | |   | | | |/ __/ _` |/ __| | |    // _ \/ __|/ _ \/ _` | '__/ __| '_ \  ||
 *  || | |___| |_| | (_| (_| | (__| | | |\ \  __/\__ \  __/ (_| | | | (__| | | | ||
 *  || \_____/\__,_|\___\__,_|\___|_| \_| \_\___||___/\___|\__,_|_|  \___|_| |_| ||
 *  ||                                                                           ||
 *  ||===========================================================================||
 *
 *
 *                                 //===========\\
 *                                //      _ ____ \\
 *                               //      | |  _ \ \\
 *                              //       | | |_) | \\
 *                              \\   __  | |  _ <  //
 *                               \\ |__| |_|_| \_\//
 *                                \\             //
 *                                 \\===========//
 *
 */

package net.lucaciresearch.cloccbot;

import net.lucaciresearch.cloccbot.config.Config;

public class Main {

    public static void main(String[] args) throws Exception {
        if (!Config.getInstance().initialize()) {
            System.exit(-1);
        }
        ApplicationContext applicationContext = new ApplicationContext();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            applicationContext.getScheduler().shutdown();
            applicationContext.getDiscord().shutdown();
        }));

        applicationContext.getScheduler().start();

    }

}