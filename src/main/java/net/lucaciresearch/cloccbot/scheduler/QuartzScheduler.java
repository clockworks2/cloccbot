/*
 * Cloccbot - Clockworks Minute Bot
 * Copyright (C) 2023-present Lucaci Research
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *  ||===========================================================================||
 *  ||  _                          _  ______                              _      ||
 *  || | |                        (_) | ___ \                            | |     ||
 *  || | |    _   _  ___ __ _  ___ _  | |_/ /___  ___  ___  __ _ _ __ ___| |__   ||
 *  || | |   | | | |/ __/ _` |/ __| | |    // _ \/ __|/ _ \/ _` | '__/ __| '_ \  ||
 *  || | |___| |_| | (_| (_| | (__| | | |\ \  __/\__ \  __/ (_| | | | (__| | | | ||
 *  || \_____/\__,_|\___\__,_|\___|_| \_| \_\___||___/\___|\__,_|_|  \___|_| |_| ||
 *  ||                                                                           ||
 *  ||===========================================================================||
 *
 *
 *                                 //===========\\
 *                                //      _ ____ \\
 *                               //      | |  _ \ \\
 *                              //       | | |_) | \\
 *                              \\   __  | |  _ <  //
 *                               \\ |__| |_|_| \_\//
 *                                \\             //
 *                                 \\===========//
 *
 */

package net.lucaciresearch.cloccbot.scheduler;

import net.lucaciresearch.cloccbot.ApplicationContext;
import net.lucaciresearch.cloccbot.config.Config;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.Set;
import java.util.function.Function;

/**
 * see https://www.baeldung.com/quartz
 */
public class QuartzScheduler {
    private static final Logger logger = LoggerFactory.getLogger(QuartzScheduler.class);

    private final Scheduler scheduler;
    private final ApplicationContext appcon;

    public QuartzScheduler(ApplicationContext applicationContext) {
        appcon = applicationContext;
        Properties properties = new Properties();
        properties.setProperty("org.quartz.jobStore.class", "org.quartz.impl.jdbcjobstore.JobStoreTX");
        properties.setProperty("org.quartz.jobStore.driverDelegateClass", "org.quartz.impl.jdbcjobstore.StdJDBCDelegate");
        properties.setProperty("org.quartz.jobStore.tablePrefix", "QRTZ_");
        properties.setProperty("org.quartz.jobStore.dataSource", "disk");
        properties.setProperty("org.quartz.threadPool.threadCount", "1");
        properties.setProperty("org.quartz.dataSource.disk.driver", "org.h2.Driver");
        properties.setProperty("org.quartz.dataSource.disk.URL", Config.getInstance().getJdbcQuartzDBString());
        properties.setProperty("org.quartz.dataSource.disk.user", "sa");
        properties.setProperty("org.quartz.dataSource.disk.password", "");
        properties.setProperty("org.quartz.dataSource.disk.maxConnections", "6");

        StdSchedulerFactory schedulerFactory = new StdSchedulerFactory();
        Scheduler sched;
        try {
            schedulerFactory.initialize(properties);
            sched = schedulerFactory.getScheduler();
            sched.setJobFactory(new ContextAwareJobFactory(appcon));
        } catch (SchedulerException e) {
            logger.error("Failed to initalize quartz");
            throw new RuntimeException(e);
        }
        scheduler = sched;
    }

    /**
     * A job is a type of event that can happen. It is defined once and fired multiple times.
     * @param jobName the job's name (identifier)
     * @param clazz the clazz of the ContextAwareJob class to be instantiated and called
     */
    public void addJob(String jobName, Class<? extends ContextAwareJob> clazz) {
        JobDetail details = JobBuilder.newJob()
                .withIdentity(jobName, "cloccbot")
                .storeDurably(true)
                .ofType(clazz)
                .build();
        try {
            scheduler.addJob(details, true);
        } catch (SchedulerException e) {
            logger.error("Failed to create a now job with name {} and class {}", jobName, clazz.getName());
            throw new RuntimeException(e);
        }
    }

    public void addTrigger(String jobName, String triggerName, JobDataMap params, Function<TriggerBuilder<Trigger>, Trigger> when) {
        var builder = TriggerBuilder.newTrigger()
                .withIdentity(triggerName, "cloccbot")
                .forJob(jobName, "cloccbot")
                .usingJobData(params);
        var trigger = when.apply(builder);
        try {
            JobDetail jd = scheduler.getJobDetail(JobKey.jobKey(jobName, "cloccbot"));
            scheduler.scheduleJob(jd, Set.of(trigger), true);
        } catch (SchedulerException e) {
            logger.error("Failed to create a new trigger with name {} for job {}", triggerName, jobName);
            throw new RuntimeException(e);
        }
    }

    public void removeTrigger(String triggerName) {
        try {
            scheduler.unscheduleJob(TriggerKey.triggerKey(triggerName, "cloccbot"));
        } catch (SchedulerException e) {
            logger.error("Failed to remove a trigger with name {}", triggerName);
            throw new RuntimeException(e);
        }
    }

    public void start() {
        try {
            scheduler.start();
        } catch (SchedulerException e) {
            logger.error("Failed to start quartz scheduler");
            throw new RuntimeException(e);
        }
    }

    public void shutdown() {
        try {
            if (scheduler.isStarted()) {
                scheduler.shutdown(true);
            }
        } catch (SchedulerException e) {
            logger.error("Failed to shutdown quartz scheduler");
            throw new RuntimeException(e);
        }
    }


}
