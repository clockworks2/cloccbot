/*
 * Cloccbot - Clockworks Minute Bot
 * Copyright (C) 2023-present Lucaci Research
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *  ||===========================================================================||
 *  ||  _                          _  ______                              _      ||
 *  || | |                        (_) | ___ \                            | |     ||
 *  || | |    _   _  ___ __ _  ___ _  | |_/ /___  ___  ___  __ _ _ __ ___| |__   ||
 *  || | |   | | | |/ __/ _` |/ __| | |    // _ \/ __|/ _ \/ _` | '__/ __| '_ \  ||
 *  || | |___| |_| | (_| (_| | (__| | | |\ \  __/\__ \  __/ (_| | | | (__| | | | ||
 *  || \_____/\__,_|\___\__,_|\___|_| \_| \_\___||___/\___|\__,_|_|  \___|_| |_| ||
 *  ||                                                                           ||
 *  ||===========================================================================||
 *
 *
 *                                 //===========\\
 *                                //      _ ____ \\
 *                               //      | |  _ \ \\
 *                              //       | | |_) | \\
 *                              \\   __  | |  _ <  //
 *                               \\ |__| |_|_| \_\//
 *                                \\             //
 *                                 \\===========//
 *
 */

package net.lucaciresearch.cloccbot.discord;

import lombok.Getter;
import lombok.Setter;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandParser {
    private static final Logger logger = LoggerFactory.getLogger(CommandParser.class);
    private static final Pattern contentExtractor = Pattern.compile("^([a-zA-Z0-9]+)(\\s+(.*))?$");

    @Getter @Setter
    private char prefix;

    private Map<String, Acceptor> acceptors = new HashMap<>();

    public CommandParser(char prefix) {
        this.prefix = prefix;
    }

    public void register(String command, Acceptor acceptor) {
        if (acceptor == null || command == null || !command.matches("^[a-zA-Z0-9]+$")) {
            logger.error("Failed to register a command: illegal arguments");
            throw new IllegalArgumentException();
        }
        acceptors.put(command.toLowerCase(), acceptor);
    }

    public void register(List<String> commands, Acceptor acceptor) {
        for (var command : commands) {
            register(command, acceptor);
        }
    }

    public boolean parse(MessageReceivedEvent msgev, boolean dryRun) {
        if (msgev == null) {
            return false;
        }
        String message = msgev.getMessage().getContentRaw();
        if (message.length() == 0 || message.charAt(0) != prefix) {
            return false;
        }
        Matcher parts = contentExtractor.matcher(message.substring(1));
        if (!parts.matches()) {
            return false;
        }
        String command = parts.group(1).toLowerCase();
        String content = "";
        try {
            content = parts.group(3);
        } catch (IndexOutOfBoundsException iobe) {
            // nothing
        }
        Acceptor acceptor = acceptors.get(command);
        if (acceptor == null) {
            return false;
        }
        if (!dryRun) {
            acceptor.accept(msgev, content, command);
        }
        return true;
    }


    public interface Acceptor {

        /**
         * Run an action for a command
         * @param originalMessageEvent the GuildMessageReceivedEvent object, as received from JDA
         * @param content the content of the message. The prefix, command and spaces following are stripped.
         * @param matchedCommand the name of the command, in all lowercase
         */
        void accept(MessageReceivedEvent originalMessageEvent, String content, String matchedCommand);
    }
}
