/*
 * Cloccbot - Clockworks Minute Bot
 * Copyright (C) 2023-present Lucaci Research
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *  ||===========================================================================||
 *  ||  _                          _  ______                              _      ||
 *  || | |                        (_) | ___ \                            | |     ||
 *  || | |    _   _  ___ __ _  ___ _  | |_/ /___  ___  ___  __ _ _ __ ___| |__   ||
 *  || | |   | | | |/ __/ _` |/ __| | |    // _ \/ __|/ _ \/ _` | '__/ __| '_ \  ||
 *  || | |___| |_| | (_| (_| | (__| | | |\ \  __/\__ \  __/ (_| | | | (__| | | | ||
 *  || \_____/\__,_|\___\__,_|\___|_| \_| \_\___||___/\___|\__,_|_|  \___|_| |_| ||
 *  ||                                                                           ||
 *  ||===========================================================================||
 *
 *
 *                                 //===========\\
 *                                //      _ ____ \\
 *                               //      | |  _ \ \\
 *                              //       | | |_) | \\
 *                              \\   __  | |  _ <  //
 *                               \\ |__| |_|_| \_\//
 *                                \\             //
 *                                 \\===========//
 *
 */

package net.lucaciresearch.cloccbot.discord;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.message.MessageDeleteEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.utils.messages.MessageEditData;
import net.lucaciresearch.cloccbot.ApplicationContext;
import net.lucaciresearch.cloccbot.Util;
import net.lucaciresearch.cloccbot.config.Config;
import net.lucaciresearch.cloccbot.data.DataRepository;
import net.lucaciresearch.cloccbot.data.entities.Minute;
import net.lucaciresearch.cloccbot.minute.EmptyMinuteException;
import net.lucaciresearch.cloccbot.minute.FatalMinuteException;
import net.lucaciresearch.cloccbot.minute.MinuteActions;
import net.lucaciresearch.cloccbot.minute.NoActiveMinuteException;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MinuteCommand extends ListenerAdapter {
    private static final Logger logger = LoggerFactory.getLogger(MinuteCommand.class);
    private static final Executor executor = Executors.newSingleThreadExecutor(r -> new Thread(r, "MinuteAdapter"));

    private final ApplicationContext appcon;
    private final Discord discord;
    private final MinuteActions minuteActions;
    private final CommandParser parser;
    private final String minuteChannel;


    public MinuteCommand(ApplicationContext appcon, Discord discord) {
        this.appcon = appcon;
        this.discord = discord;
        this.minuteActions = appcon.getMinuteActions();
        minuteChannel = Config.getInstance().getDiscordChannelMapping().getMinutesId(); // TODO extract from Discord


        /***
         * Maybe add another parser with command /title to change the title of the minute while writing it. Delete the /title and send a "Title set to: ..."
         *
         * Why another parser? because using the same parser would break .matchesCommand in lines 65-75
         * The title should be reseted when a minute is finished
         *
         * Also reset the title after ~6h to prevent it from bleeding into another minute.
         * Don't forget to cancel existing reset timers, otherwise multiple resets could happen.
         *
         * After this is implemented, we could make /endminute refuse if no title is given as parameter and no /title is active
         */

        /***
         *     _____ _   _ _____ _____    _____ ___________ _____
         *    |_   _| | | |_   _/  ___|  /  __ \  _  |  _  \  ___|
         *      | | | |_| | | | \ `--.   | /  \/ | | | | | | |__
         *      | | |  _  | | |  `--. \  | |   | | | | | | |  __|
         *      | | | | | |_| |_/\__/ /  | \__/\ \_/ / |/ /| |___
         *      \_/ \_| |_/\___/\____/    \____/\___/|___/ \____/
         *
         *
         *     _____ _____    _____ _   _ _____ _____
         *    |_   _/  ___|  /  ___| | | |_   _|_   _|
         *      | | \ `--.   \ `--.| |_| | | |   | |
         *      | |  `--. \   `--. \  _  | | |   | |
         *     _| |_/\__/ /  /\__/ / | | |_| |_  | |
         *     \___/\____/   \____/\_| |_/\___/  \_/
         *
         *
         *     _                            _     _          _                   _         _ _   _ _ _   _                    _
         *    | |                          | |   | |        | |                 | |       (_) | ( ) | | | |                  | |
         *    | |_ _ __ _   _   _ __   ___ | |_  | |_ ___   | |_ ___  _   _  ___| |__      _| |_|/| | | | |__  _ __ ___  __ _| | __
         *    | __| '__| | | | | '_ \ / _ \| __| | __/ _ \  | __/ _ \| | | |/ __| '_ \    | | __| | | | | '_ \| '__/ _ \/ _` | |/ /
         *    | |_| |  | |_| | | | | | (_) | |_  | || (_) | | || (_) | |_| | (__| | | |_  | | |_  | | | | |_) | | |  __/ (_| |   <
         *     \__|_|   \__, | |_| |_|\___/ \__|  \__\___/   \__\___/ \__,_|\___|_| |_( ) |_|\__| |_|_| |_.__/|_|  \___|\__,_|_|\_\
         *               __/ |                                                        |/
         *              |___/
         *                            -- lucaci32u4, the author of this shit code
         *
         *                            todo: test corner cases.
         *                            close with all X
         *                            close with all delete
         *                            autoclose witl all x
         *                            autoclose with all delete
         *
         */

        parser = new CommandParser('/');

        parser.register(List.of("end", "done"), (originalMessageEvent, content, matchedCommand) -> {
            makeMinute(originalMessageEvent, content);
        });
        parser.register("title", (originalMessageEvent, content, matchedCommand) -> {
            DataRepository dr = appcon.getDataFactory().create();
            var minute = dr.getLatestActiveMinute();
            dr.close();
            if (minute.isEmpty()) {
                return;
            }
            minuteActions.changeMinuteTitle(minute.get().getId(), content).thenComposeAsync(_u -> {
                return minuteActions.attachAuxiliaryMessage(minute.get().getId(), printTitleSetTo(content));
            }, executor).thenAccept(message -> {
                // TODO: schedule message for deletion 30 mins later
            });
        });
        parser.register("cancel", (originalMessageEvent, content, matchedCommand) -> {
            originalMessageEvent.getMessage().delete().queue();
            DataRepository dataRepository = appcon.getDataFactory().create();
            dataRepository.getLatestActiveMinute().ifPresent(minute -> {
                MessageEmbed cancelDone = new EmbedBuilder().setColor(Color.GREEN).setDescription("Cancelled.\nStart typing to create a new minute.").build();
                var cfProgress = printWorkingOnIt();
                var cfCancel = minuteActions.cancelMinute(minute.getId());
                cfCancel.thenComposeAsync(_u -> cfProgress, executor).thenAcceptAsync(prog -> {
                    prog.delete().queue();
                    Util.sendMessage(discord.getMinuteChannel(), cancelDone);
                }, executor);
            });
            dataRepository.close();
        });
    }

    @Override
    public void onMessageDelete(@NotNull MessageDeleteEvent event) {
        // TODO: remove message & update begin message
    }

    @Override
    public void onMessageReceived(@NotNull MessageReceivedEvent event) {

        // if not on #minutes, drop
        if (!event.getChannel().getId().equals(minuteChannel)) {
            return;
        }

        // if command, drop
        boolean consumed = parser.parse(event, false);
        if (consumed) {
            return;
        }


        // if not bot, add to minute
        if (event.getJDA().getSelfUser().getIdLong() != event.getAuthor().getIdLong()) {

            CompletableFuture<Minute> cfMinute = minuteActions.getLatestMinute().exceptionallyComposeAsync(throwable -> {
                if (throwable.getCause() instanceof NoActiveMinuteException) {
                    var cfMinId = minuteActions.createNewMinute(event.getMessageIdLong());
                    return cfMinId.thenComposeAsync(min -> {
                        return minuteActions.attachAuxiliaryMessageAsBegin(min.getId(), Util.sendMessage(discord.getMinuteChannel(), createBeginMessage(1))).thenApplyAsync(_u -> min, executor);
                    }, executor);
                }
                throw new RuntimeException(throwable);
            }, executor);

            var cfAdded = cfMinute.thenComposeAsync(minute -> {
                return minuteActions.attachMessage(minute.getId(), event.getMessage(), this::editBeginMessage);
            }, executor);

            cfAdded.exceptionallyAsync(throwable -> {
                // TODO: print a big error to #programare
                return null;
            }, executor);

        }
    }

    private void makeMinute(@NotNull MessageReceivedEvent event, String title) {

        event.getMessage().delete().queue();

        CompletableFuture<Minute> cfMinute = minuteActions.getLatestMinute();

        cfMinute.exceptionallyAsync(throwable -> {
            if (throwable instanceof NoActiveMinuteException) {
                printNoContentWarning();
            }
            return null;
        }, executor);

        var cfWorkingOnit = printWorkingOnIt();

        var cfWikiUrl = cfMinute.thenComposeAsync(minute -> {

            // change title id needed
            boolean changeTitle = title != null && !title.isEmpty() && !title.isBlank();
            CompletableFuture<Void> cfTitleChange = changeTitle ? minuteActions.changeMinuteTitle(minute.getId(), title) : CompletableFuture.completedFuture(null);

            // upload to wiki and shorten
            CompletableFuture<String> wikiUrl = cfTitleChange.thenComposeAsync(_u -> {
                return minuteActions.makeMinute(minute.getId());
            }, executor).thenComposeAsync(longUrl -> {
                return appcon.getUrlShortener().shorten(longUrl);
            }, executor);

            return wikiUrl;
        }, executor);


        cfMinute.thenComposeAsync(minute -> {
            return cfWikiUrl.thenAcceptAsync(url -> {
                String message = "I've created a new minute.\n Here's the link: " + url;
                EmbedBuilder embedBuilder = new EmbedBuilder().setColor(Color.GREEN).setDescription(message);
                discord.getMinuteChannel().sendMessageEmbeds(embedBuilder.build()).queue();
                discord.getGeneralChannel().sendMessageEmbeds(embedBuilder.build()).queue();
            }, executor);
        }, executor);

        cfMinute.thenComposeAsync(minute -> {
            return cfWikiUrl.exceptionallyAsync(throwable -> {
                if (throwable.getCause() instanceof EmptyMinuteException) {
                    minuteActions.attachAuxiliaryMessage(minute.getId(), printNoContentWarning());
                    return null;
                }

                String message = "An unknown error happened while creating the minute.\nPlease tell #programare to check logs.";
                if (throwable.getCause() instanceof FatalMinuteException) {
                    message = "An error happened while creating the minute:\n" + throwable.getCause().getMessage();
                }
                MessageEmbed embedBuilder = new EmbedBuilder().setColor(Color.GREEN).setDescription(message).build();
                minuteActions.attachAuxiliaryMessage(minute.getId(), Util.sendMessage(discord.getMinuteChannel(), embedBuilder));
                Util.sendMessage(discord.getProgrammingChannel(), embedBuilder);

                logger.error(message, throwable);
                return null;
            }, executor);
        }, executor);

        cfMinute.thenComposeAsync(minute -> {
            return cfWikiUrl.handleAsync((_url, _exc) -> null, executor).thenComposeAsync(_u -> cfWorkingOnit, executor).thenAcceptAsync(message -> {
                message.delete().queue();
            }, executor);
        }, executor);

    }

    private CompletableFuture<Message> printNoContentWarning() {
        MessageEmbed msg = new EmbedBuilder().setColor(Color.RED).setDescription("I don't have any content!").build();
        var cfMsg = Util.sendMessage(discord.getMinuteChannel(), msg);

        cfMsg.thenAcceptAsync(message -> {
            // TODO: set a timer 30 secs to call message.delete().queue()
        }, executor);

        return cfMsg;
    }

    private CompletableFuture<Message> printWorkingOnIt() {
        MessageEmbed msg = new EmbedBuilder().setColor(Color.ORANGE).setDescription("Working on it...").build();
        return Util.sendMessage(discord.getMinuteChannel(), msg);
    }

    private MessageEmbed createBeginMessage(int messageCount) {
        EmbedBuilder msg = new EmbedBuilder();
        msg.setColor(Color.ORANGE);
        msg.setTitle("New minute started");
        msg.appendDescription("You can start writing the messages here and I will turn them into a nice wiki page").appendDescription("\n");
        msg.addField("Finish minute", "/end or /done", true);
        msg.addField("Cancel", "/cancel", true);
        msg.addField("Set a title", "/title <title>", true);
        msg.setFooter("Message count: " + messageCount);
        return msg.build();
    }

    private CompletableFuture<Void> editBeginMessage(Minute min) {
        MessageEmbed embed = createBeginMessage((int) min.streamEligibleMessages().count());
        if (min.getInitialBotMessageId().isEmpty()) {
            return CompletableFuture.completedFuture(null);
        }
        return discord.getMinuteChannel().editMessageById(min.getInitialBotMessageId().get(), MessageEditData.fromEmbeds(embed)).submit().thenApplyAsync(_m -> null, executor);
    }

    private CompletableFuture<Message> printTitleSetTo(String title) {
        MessageEmbed msg = new EmbedBuilder().setColor(Color.ORANGE).setTitle("Title set to").setDescription(title).build();
        return Util.sendMessage(discord.getMinuteChannel(), msg);
    }

}
