/*
 * Cloccbot - Clockworks Minute Bot
 * Copyright (C) 2023-present Lucaci Research
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *  ||===========================================================================||
 *  ||  _                          _  ______                              _      ||
 *  || | |                        (_) | ___ \                            | |     ||
 *  || | |    _   _  ___ __ _  ___ _  | |_/ /___  ___  ___  __ _ _ __ ___| |__   ||
 *  || | |   | | | |/ __/ _` |/ __| | |    // _ \/ __|/ _ \/ _` | '__/ __| '_ \  ||
 *  || | |___| |_| | (_| (_| | (__| | | |\ \  __/\__ \  __/ (_| | | | (__| | | | ||
 *  || \_____/\__,_|\___\__,_|\___|_| \_| \_\___||___/\___|\__,_|_|  \___|_| |_| ||
 *  ||                                                                           ||
 *  ||===========================================================================||
 *
 *
 *                                 //===========\\
 *                                //      _ ____ \\
 *                               //      | |  _ \ \\
 *                              //       | | |_) | \\
 *                              \\   __  | |  _ <  //
 *                               \\ |__| |_|_| \_\//
 *                                \\             //
 *                                 \\===========//
 *
 */

package net.lucaciresearch.cloccbot.discord;

import lombok.Getter;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.lucaciresearch.cloccbot.ApplicationContext;
import net.lucaciresearch.cloccbot.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.security.auth.login.LoginException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Discord {
    private static final Logger logger = LoggerFactory.getLogger(Discord.class);

    private final ApplicationContext context;

    @Getter
    private final JDA jda;

    @Getter
    private final TextChannel minuteChannel;

    @Getter
    private final TextChannel generalChannel;

    @Getter
    private final TextChannel announcementsChannel;

    @Getter
    private final TextChannel programmingChannel;

    @Getter
    private final MinuteCommand minuteCommandListener;

    public Discord(ApplicationContext context) throws LoginException {
        this.context = context;

        JDABuilder builder = JDABuilder.createDefault(Config.getInstance().getDiscordToken());
        builder.setEnabledIntents(Arrays.asList(GatewayIntent.values()));

        List<GatewayIntent> temp = new ArrayList<>(); // mark daca vezi asta
        temp.add(GatewayIntent.GUILD_MEMBERS); // da enable din site la chestiile astea
        temp.add(GatewayIntent.GUILD_PRESENCES); // 123
        temp.add(GatewayIntent.MESSAGE_CONTENT);
        temp.add(GatewayIntent.GUILD_MESSAGE_REACTIONS);
        temp.add(GatewayIntent.GUILD_EMOJIS_AND_STICKERS);
        builder.enableIntents(temp); // alex: is this still needed?

        jda = builder.setActivity(Activity.of(Activity.ActivityType.WATCHING, "your minutes")).build();

        try {
            jda.awaitReady();
            minuteChannel = jda.getTextChannelById(Config.getInstance().getDiscordChannelMapping().getMinutesId());
            generalChannel = jda.getTextChannelById(Config.getInstance().getDiscordChannelMapping().getGeneralId());
            announcementsChannel = jda.getTextChannelById(Config.getInstance().getDiscordChannelMapping().getAnnouncementsId());
            programmingChannel = jda.getTextChannelById(Config.getInstance().getDiscordChannelMapping().getProgramareId());
            if (minuteChannel == null) throw new IllegalArgumentException("Missing minute channel");
            if (generalChannel == null) throw new IllegalArgumentException("Missing general channel");
            if (announcementsChannel == null) throw new IllegalArgumentException("Missing announcements channel");
            if (programmingChannel == null) throw new IllegalArgumentException("Missing programming channel");

            minuteCommandListener = new MinuteCommand(context, this);


            jda.addEventListener(minuteCommandListener);
        } catch (InterruptedException iex) {
            logger.error("Interrupted while waiting for JDA to connect");
            throw new RuntimeException();
        }
    }

    public void shutdown() {
        jda.shutdown();
    }
}
