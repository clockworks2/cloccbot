/*
 * Cloccbot - Clockworks Minute Bot
 * Copyright (C) 2023-present Lucaci Research
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *  ||===========================================================================||
 *  ||  _                          _  ______                              _      ||
 *  || | |                        (_) | ___ \                            | |     ||
 *  || | |    _   _  ___ __ _  ___ _  | |_/ /___  ___  ___  __ _ _ __ ___| |__   ||
 *  || | |   | | | |/ __/ _` |/ __| | |    // _ \/ __|/ _ \/ _` | '__/ __| '_ \  ||
 *  || | |___| |_| | (_| (_| | (__| | | |\ \  __/\__ \  __/ (_| | | | (__| | | | ||
 *  || \_____/\__,_|\___\__,_|\___|_| \_| \_\___||___/\___|\__,_|_|  \___|_| |_| ||
 *  ||                                                                           ||
 *  ||===========================================================================||
 *
 *
 *                                 //===========\\
 *                                //      _ ____ \\
 *                               //      | |  _ \ \\
 *                              //       | | |_) | \\
 *                              \\   __  | |  _ <  //
 *                               \\ |__| |_|_| \_\//
 *                                \\             //
 *                                 \\===========//
 *
 */

package net.lucaciresearch.cloccbot.minute;

import net.lucaciresearch.cloccbot.ApplicationContext;
import net.lucaciresearch.cloccbot.data.entities.Minute;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class MinuteBuilder {
    private static final Logger logger = LoggerFactory.getLogger(MinuteBuilder.class);
    private static final Executor executorService = Executors.newSingleThreadExecutor(r -> new Thread(r, "MinuteBuilder"));

    private static final Pattern imageExtensionRegex = Pattern.compile("^(gif|jpe?g|tiff?|png|webp|bmp|ico)$");

    private final ApplicationContext appcon;

    private String minutePath;
    private String header;
    private List<MinuteComponent> components = new ArrayList<>();

    protected MinuteBuilder(ApplicationContext appcon) {
        this.appcon = appcon;
    }

    /**
     * Give the minute a name and filename
     * @param minute the minute object to transform to markdown archive rep
     */
    public MinuteBuilder withTitle(Minute minute) {
        this.minutePath = minute.getFilename();

        StringBuilder headerBuilder = new StringBuilder();

        headerBuilder.setLength(0);
        headerBuilder.append("---\n");
        headerBuilder.append("layout: default\n");
        headerBuilder.append("title: ").append(minute.getTitle()).append("\n");
        headerBuilder.append("has_children: false\n");
        headerBuilder.append("parent: Minutes\n");
        headerBuilder.append("nav_order: ").append(minute.getNavigationOrderIndex()).append("\n");
        headerBuilder.append("---\n");
        headerBuilder.append("\n");
        headerBuilder.append("# ").append(minute.getTitle()).append("\n");
        headerBuilder.append("\n");

        header = headerBuilder.toString();

        return this;
    }

    public MinuteBuilder withText(String text) {
        while (text.endsWith("\n")) text = text.substring(0, text.length() - 1);
        components.add(new TextComponent(text));
        return this;
    }

    public MinuteBuilder withImage(String url) {
        components.add(new ImageComponent(url));
        return this;
    }

    public CompletableFuture<MinuteZipData> build() {
        return CompletableFuture.supplyAsync(() -> {
            StringBuilder sb = new StringBuilder(header);
            Map<String, byte[]> images = new HashMap<>();
            int imageCounter = 0;
            for (MinuteComponent mncmp : components) {

                // Text components
                if (mncmp instanceof TextComponent text) {
                    sb.append(text.markdownString());
                }

                // Image components. Hell goes here
                if (mncmp instanceof ImageComponent image) {
                    imageCounter++;
                    try {
                        // Validate URL
                        URL url = new URL(image.url());
                        String extension = FilenameUtils.getExtension(url.getPath());
                        if (!imageExtensionRegex.matcher(extension.toLowerCase()).matches()) {
                            throw new MinuteException("URL has invalid extension");
                        }

                        // Get image contents
                        String imageFilename = "image" + imageCounter + "." + extension;
                        Request getImageReq = new Request.Builder()
                                .url(image.url())
                                .get().build();
                        Response getPhotoResp = appcon.getHttpClient().newCall(getImageReq).execute();
                        if (getPhotoResp.code() != 200 || getPhotoResp.body() == null) {
                            getPhotoResp.close();
                            throw new MinuteException("GET request did not return valid response");
                        }
                        images.put(imageFilename, getPhotoResp.body().bytes());
                        getPhotoResp.close();

                        // Write markdown
                        sb.append("![Image ").append(imageCounter).append("](").append(imageFilename).append(")");
                    } catch (Exception e) {
                        // fallback: include image as link
                        if (e instanceof MinuteException mex) {
                            logger.warn("Failed to include an image into minute {} because {}", minutePath, mex.getMessage());
                        } else {
                            logger.warn("Failed to include an image into minute {} with unknown reason: {}", minutePath, e.toString());
                        }
                        sb.append("[Image ").append(imageCounter).append("](").append(image.url()).append(")");
                    }
                }

                // Write newlines
                sb.append("\n\n");
            }

            // Write zip file
            ByteArrayOutputStream binOutputStream = new ByteArrayOutputStream();
            try (ZipOutputStream zos = new ZipOutputStream(binOutputStream)) {
                for (Map.Entry<String, byte[]> e : images.entrySet()) {
                    String name = e.getKey();
                    byte[] content = e.getValue();
                    ZipEntry entry = new ZipEntry(name);
                    zos.putNextEntry(entry);
                    zos.write(content);
                    zos.closeEntry();
                }
                ZipEntry entry = new ZipEntry("index.markdown");
                zos.putNextEntry(entry);
                zos.write(sb.toString().getBytes());
                zos.closeEntry();
            } catch (IOException ioe) {
                logger.error("Fatal error creating writing zip file", ioe);
                throw new FatalMinuteException("Could not create zip file", ioe);
            }

            return new MinuteZipData(appcon, binOutputStream.toByteArray(), minutePath);
        }, executorService);
    }


}

interface MinuteComponent { }

record TextComponent(String markdownString) implements MinuteComponent { }

record ImageComponent(String url) implements MinuteComponent { }
