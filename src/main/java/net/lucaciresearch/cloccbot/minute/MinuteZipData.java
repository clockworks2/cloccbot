/*
 * Cloccbot - Clockworks Minute Bot
 * Copyright (C) 2023-present Lucaci Research
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *  ||===========================================================================||
 *  ||  _                          _  ______                              _      ||
 *  || | |                        (_) | ___ \                            | |     ||
 *  || | |    _   _  ___ __ _  ___ _  | |_/ /___  ___  ___  __ _ _ __ ___| |__   ||
 *  || | |   | | | |/ __/ _` |/ __| | |    // _ \/ __|/ _ \/ _` | '__/ __| '_ \  ||
 *  || | |___| |_| | (_| (_| | (__| | | |\ \  __/\__ \  __/ (_| | | | (__| | | | ||
 *  || \_____/\__,_|\___\__,_|\___|_| \_| \_\___||___/\___|\__,_|_|  \___|_| |_| ||
 *  ||                                                                           ||
 *  ||===========================================================================||
 *
 *
 *                                 //===========\\
 *                                //      _ ____ \\
 *                               //      | |  _ \ \\
 *                              //       | | |_) | \\
 *                              \\   __  | |  _ <  //
 *                               \\ |__| |_|_| \_\//
 *                                \\             //
 *                                 \\===========//
 *
 */

package net.lucaciresearch.cloccbot.minute;

import net.lucaciresearch.cloccbot.ApplicationContext;
import net.lucaciresearch.cloccbot.config.Config;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public record MinuteZipData(ApplicationContext applicationContext, byte[] zipContent, String pathSegment) {
    private static final Logger logger = LoggerFactory.getLogger(MinuteZipData.class);

    /**
     * Upload the minute to wiki
     * @return the URL of the uploaded minute
     */
    public String upload() {
        RequestBody body = RequestBody.create(MediaType.get("application/zip"), zipContent);
        Request request = new Request.Builder()
                .url(Config.getInstance().getWikiUploadUrl())
                .addHeader("UnpackAuth", Config.getInstance().getWikiUploadToken())
                .addHeader("UnpackLocation", pathSegment)
                .post(body)
                .build();
        try {
            Response response = applicationContext.getHttpClient().newCall(request).execute();
            response.close();
            if (response.code() != 200) {
                String msg = "Failed to upload minute to wiki pages. Got HTTP " + response.code();
                logger.error(msg);
                throw new FatalMinuteException(msg);
            }
        } catch (IOException exception) {
            logger.error("Unknown exception while uploading minute to wiki pages");
            throw new FatalMinuteException("Failed to upload minute to wiki pages", exception);
        }
        return Config.getInstance().getMinuteUrlPrefix() + pathSegment + "/";
    }

}
