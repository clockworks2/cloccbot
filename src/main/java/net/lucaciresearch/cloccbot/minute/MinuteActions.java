/*
 * Cloccbot - Clockworks Minute Bot
 * Copyright (C) 2023-present Lucaci Research
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *  ||===========================================================================||
 *  ||  _                          _  ______                              _      ||
 *  || | |                        (_) | ___ \                            | |     ||
 *  || | |    _   _  ___ __ _  ___ _  | |_/ /___  ___  ___  __ _ _ __ ___| |__   ||
 *  || | |   | | | |/ __/ _` |/ __| | |    // _ \/ __|/ _ \/ _` | '__/ __| '_ \  ||
 *  || | |___| |_| | (_| (_| | (__| | | |\ \  __/\__ \  __/ (_| | | | (__| | | | ||
 *  || \_____/\__,_|\___\__,_|\___|_| \_| \_\___||___/\___|\__,_|_|  \___|_| |_| ||
 *  ||                                                                           ||
 *  ||===========================================================================||
 *
 *
 *                                 //===========\\
 *                                //      _ ____ \\
 *                               //      | |  _ \ \\
 *                              //       | | |_) | \\
 *                              \\   __  | |  _ <  //
 *                               \\ |__| |_|_| \_\//
 *                                \\             //
 *                                 \\===========//
 *
 */

package net.lucaciresearch.cloccbot.minute;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageReaction;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.lucaciresearch.cloccbot.ApplicationContext;
import net.lucaciresearch.cloccbot.Util;
import net.lucaciresearch.cloccbot.config.Config;
import net.lucaciresearch.cloccbot.data.DataRepository;
import net.lucaciresearch.cloccbot.data.DataRepositoryFactory;
import net.lucaciresearch.cloccbot.data.entities.Minute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.InvalidParameterException;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MinuteActions {
    private static final Logger logger = LoggerFactory.getLogger(MinuteActions.class);

    private final ApplicationContext appcon;
    private final DataRepositoryFactory dataRepositoryFactory;
    private final Executor executor = Executors.newSingleThreadExecutor(r -> new Thread(r, "MinuteActions"));
    private final String emoteNoName = Config.getInstance().getEmotes().getCheckNo();

    public MinuteActions(ApplicationContext appcon) {
        this.appcon = appcon;
        dataRepositoryFactory = appcon.getDataFactory();
    }

    public CompletableFuture<Minute> getLatestMinute() {
        return CompletableFuture.supplyAsync(() -> {
            DataRepository dr = dataRepositoryFactory.create();
            Optional<Minute> optminute = dr.getLatestActiveMinute();
            Minute min = optminute.orElse(null);
            dr.close();
            if (min == null) throw new NoActiveMinuteException();
            return min;
        }, executor);
    }

    public CompletableFuture<Minute> createNewMinute(long initiatorId) {
        return CompletableFuture.supplyAsync(() -> {
            DataRepository dr = dataRepositoryFactory.create();
            dr.beginTransaction();
            Minute minute = dr.createMinute(() -> new Minute(initiatorId, appcon));
            dr.commitTransaction();
            dr.close();
            return minute;
        }, executor);
    }

    public CompletableFuture<Void> attachMessage(String minuteId, Message message, Function<Minute, CompletableFuture<Void>> initialMessageEditorByCount) {
        return CompletableFuture.supplyAsync(() -> {
            try (DataRepository dr = dataRepositoryFactory.create()) {
                Minute min = dr.getMinuteById(minuteId).orElseThrow(NoActiveMinuteException::new);
                dr.beginTransaction();
                min.addEligibleMessage(message.getIdLong());
                dr.commitTransaction();
                return min;
            }
        }, executor).thenComposeAsync(initialMessageEditorByCount);
    }

    public CompletableFuture<Message> attachAuxiliaryMessage(String minuteId, CompletableFuture<Message> message) {
        return message.thenApplyAsync(msg -> {
            try (DataRepository dr = dataRepositoryFactory.create()) {
                dr.getMinuteById(minuteId).ifPresent(minute -> {
                    if (minute.isActive()) {
                        dr.beginTransaction();
                        minute.addAuxiliraryMessage(msg.getIdLong());
                        dr.commitTransaction();
                    } else {
                        msg.delete().queue();
                    }

                });
            }
            return msg;
        }, executor);
    }

    public CompletableFuture<Void> attachAuxiliaryMessageAsBegin(String minuteId, CompletableFuture<Message> message) {
        return message.thenAcceptAsync(msg -> {
            try (DataRepository dr = dataRepositoryFactory.create()) {
                dr.getMinuteById(minuteId).ifPresent(minute -> {
                    if (minute.isActive()) {
                        dr.beginTransaction();
                        minute.setInitialBotMessageId(msg.getIdLong());
                        dr.commitTransaction();
                    } else {
                        msg.delete().queue();
                    }

                });
            }
        }, executor);
    }


    public CompletableFuture<Void> changeMinuteTitle(String minuteId, String title) {
        return CompletableFuture.runAsync(() -> {
            DataRepository dataRepository = dataRepositoryFactory.create();
            dataRepository.beginTransaction();
            dataRepository.getMinuteById(minuteId)
                    .orElseThrow(() -> new InvalidParameterException("No such minute ID"))
                    .setTitle(title);
            dataRepository.commitTransaction();
            dataRepository.close();
        }, executor);
    }

    public CompletableFuture<String> makeMinute(String minuteId) {

        DataRepository dr = dataRepositoryFactory.create();
        Minute minute = dr.getMinuteById(minuteId).orElseThrow(() -> new InvalidParameterException("No such minute ID"));
        dr.close();

        MinuteBuilder mb = new MinuteBuilder(appcon);
        mb.withTitle(minute);

        AtomicBoolean hasContent = new AtomicBoolean(false);

        var cfUrl = forEachMinuteMessage(minute.streamEligibleMessages(), message -> {
            if (Util.hasEmote(message, emoteNoName)) {
                return;
            }
            String minuteContent = message.getContentRaw();
            if (!minuteContent.isBlank()) {
                mb.withText(minuteContent);
                hasContent.set(true);
            }
            for (var att : message.getAttachments()) {
                if (att.isImage()) {
                    mb.withImage(att.getProxyUrl());
                    hasContent.set(true);
                }
            }
        }, throwable -> {
            // message is deleted. ignore.
        }).thenComposeAsync(_u -> {
            if (!hasContent.get()) {
                throw new EmptyMinuteException();
            }
            return mb.build();
        }, executor).thenApplyAsync(MinuteZipData::upload, executor);

        var cfConfirmOk = cfUrl.thenApplyAsync(wikiUrl -> {
            DataRepository ddr = dataRepositoryFactory.create();
            Optional<Minute> optmin = ddr.getMinuteById(minuteId);
            if (optmin.isEmpty()) {
                logger.error("Minute disapeared from database while confirming the OK status");
                throw new RuntimeException();
            }
            Minute min = optmin.get();
            ddr.beginTransaction();
            min.succeed();
            ddr.commitTransaction();
            ddr.close();
            return min;
        }, executor);

        cfConfirmOk.thenComposeAsync(min -> {
            return deleteBulk(min.streamAuxiliaryMessages());
        }, executor);

        cfConfirmOk.thenComposeAsync(min -> {
            return forEachMinuteMessage(min.streamEligibleMessages(), message -> {
                for (MessageReaction reaction : message.getReactions()) {
                    reaction.clearReactions().queue();
                }
            }, throwable -> {
                // message doe not exist anymore
            });
        }, executor);

        return cfUrl;
    }

    public CompletableFuture<Void> cancelMinute(String minuteId) {
        return CompletableFuture.supplyAsync(() -> {
            DataRepository dr = dataRepositoryFactory.create();
            Minute minute = dr.getMinuteById(minuteId).orElseThrow(() -> new InvalidParameterException("No such minute ID"));
            dr.beginTransaction();
            minute.cancel();
            dr.commitTransaction();
            dr.close();

            // do some cleanup
            return minute;
        }, executor).thenComposeAsync(minute -> {
            return deleteBulk(minute.streamEligibleMessages()).handleAsync((_u, _t) -> minute, executor);
        }, executor).thenComposeAsync(minute -> {
            return deleteBulk(minute.streamAuxiliaryMessages());
        }, executor);
    }

    private CompletableFuture<Void> forEachMinuteMessage(Stream<Long> msgStream, Consumer<Message> success, Consumer<? super Throwable> failure) {
        return CompletableFuture.runAsync(() -> {
            TextChannel channel = appcon.getDiscord().getMinuteChannel();
            AtomicInteger requiredSemaphoresTokens = new AtomicInteger(0);
            Semaphore semaphore = new Semaphore(0);

            msgStream.forEach(id -> {
                requiredSemaphoresTokens.incrementAndGet();
                channel.retrieveMessageById(id).queue(message -> {
                    success.accept(message);
                    semaphore.release();
                }, throwable -> {
                    failure.accept(throwable);
                    semaphore.release();
                });
            });
            semaphore.acquireUninterruptibly(requiredSemaphoresTokens.get());
        }, executor);
    }

    private CompletableFuture<Void> deleteBulk(Stream<Long> idStream) {
        List<String> ids = idStream.map(Object::toString).collect(Collectors.toList());
        CompletableFuture<Void> accumulator = CompletableFuture.completedFuture(null);
        while (!ids.isEmpty()) {
            CompletableFuture<Void> op;
            if (ids.size() == 1) {
                op = appcon.getDiscord().getMinuteChannel().deleteMessageById(ids.get(0)).submit();
                ids.clear();
            } else {
                var partition = ids.stream().limit(100).collect(Collectors.toList());
                op = appcon.getDiscord().getMinuteChannel().deleteMessagesByIds(partition).submit();
                ids.removeAll(partition);
            }
            CompletableFuture<Void> safeOp = op.handleAsync((_u, _t) -> null, executor);
            accumulator = accumulator.thenComposeAsync(_u -> safeOp, executor);
        }
        return accumulator;
    }

}
