/*
 * Cloccbot - Clockworks Minute Bot
 * Copyright (C) 2023-present Lucaci Research
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 *
 *  ||===========================================================================||
 *  ||  _                          _  ______                              _      ||
 *  || | |                        (_) | ___ \                            | |     ||
 *  || | |    _   _  ___ __ _  ___ _  | |_/ /___  ___  ___  __ _ _ __ ___| |__   ||
 *  || | |   | | | |/ __/ _` |/ __| | |    // _ \/ __|/ _ \/ _` | '__/ __| '_ \  ||
 *  || | |___| |_| | (_| (_| | (__| | | |\ \  __/\__ \  __/ (_| | | | (__| | | | ||
 *  || \_____/\__,_|\___\__,_|\___|_| \_| \_\___||___/\___|\__,_|_|  \___|_| |_| ||
 *  ||                                                                           ||
 *  ||===========================================================================||
 *
 *
 *                                 //===========\\
 *                                //      _ ____ \\
 *                               //      | |  _ \ \\
 *                              //       | | |_) | \\
 *                              \\   __  | |  _ <  //
 *                               \\ |__| |_|_| \_\//
 *                                \\             //
 *                                 \\===========//
 *
 */

package net.lucaciresearch.cloccbot.urls;

import net.lucaciresearch.cloccbot.ApplicationContext;
import net.lucaciresearch.cloccbot.config.Config;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class UrlShortener {
    private final static Logger logger = LoggerFactory.getLogger(UrlShortener.class);

    private final ApplicationContext appcon;
    private final Executor executor = Executors.newSingleThreadExecutor(r -> new Thread(r, "UrlShortener"));

    public UrlShortener(ApplicationContext appcon) {
        this.appcon = appcon;
    }

    public CompletableFuture<String> shorten(String source) {
        return CompletableFuture.supplyAsync(() -> {
            return source;
            /*RequestBody body = RequestBody.create(MediaType.get("text/plain"), source);
            Request request = new Request.Builder()
                    .url(Config.getInstance().getShortenerUrl())
                    .post(body)
                    .build();
            try {
                Response response = appcon.getHttpClient().newCall(request).execute();
                if (response.code() != 200 || response.body() == null) {
                    response.close();
                    logger.error("Failed to shorten URL. Got HTTP {}", response.code());
                    return source;
                }
                String shortUrl = response.body().string();
                response.close();
                return shortUrl;
            } catch (IOException exception) {
                logger.error("Unknown exception while shortening");
                return source;
            }*/
        }, executor);
    }


}
